describe('Sanity check', () => {
  it('works', () => {
    const ACTUAL = (2 + 2);
    const EXPECTED = 4;
    expect(ACTUAL).toEqual(EXPECTED);
  });
});
