# CodePlaygroundFun

aka Hangry 🍽😡

_putting the **ACK!** in webpack_

## Goals

This project is a restaurant chooser for officemates who suck at choosing
restaurants to go to on their own.

But it also an exercise in writing a node app from scratch with no generators,
scaffolds, CLIs, etc. The objective is that you will learn to hold _your own_
hand.

We will learn and configure by hand:

- webpack

- vue

- jest

- travis ci

and more! stay tuned!

## How To

1. `npm istall` -- install dependencies

2. `npm run dev` -- start a dev server

3. get to hacking
