# webpack

Configuring webpack by hand

## resources

- https://hackernoon.com/webpack-3-quickstarter-configure-webpack-from-scratch-30a6c394038a

## gotchas

- babel was crashing the dev server for a while until we put an empty object in `.babelrc`
