# docs/README

this is where the project will be documented. Since this is a learning
exercise, you will find links to resources we used along the way, and also
notes on stuff we discovered on our own. The hard way.

## random learnings and stuff

- Q. What does the `rc` in config files (e.g. `babelrc`, `bashrc`, etc) stand for?

    A. We don't know! Maybe it stands for `runcom` as suggested in [this SO
       answer](https://stackoverflow.com/questions/11030552/what-does-rc-mean-in-dot-files).
       just assume whenever you see it that it means that you're looking at a
       config file.
